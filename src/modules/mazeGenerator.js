const randomInt = require('random-int');
/* learnt from http://www.migapro.com/depth-first-search/ */

const generateMaze = (colTotal, rowTotal, initCol = false, initRow = false) => {
  let maze = [];
  for (let i = 0; i < rowTotal; i++) {
    maze[i] = [];
    for (let j = 0; j < colTotal; j++) {
      maze[i][j] = 1;
    }
  }

  // random starting cell, if not set
  if(!initCol && !initRow){
    initCol = randomInt(colTotal - 1);
    while (initCol % 2 == 0) {
      initCol = randomInt(colTotal - 1);
    }
    initRow = randomInt(rowTotal - 1);
    while (initRow % 2 == 0) {
      initRow = randomInt(rowTotal - 1);
    }
  }

  maze[initRow][initCol] = 0;

  mazeRecursion(maze, initCol, initRow);

  return maze;
}

const mazeRecursion = (maze, col, row) => {
  const arrayCellIsUndefined = (arr, index1, index2) => {
    if( typeof arr[index1] !== 'undefined' ) {
      if( typeof arr[index1][index2] !== 'undefined' ) {
        return false;
      }
    }
    return true;
  }
  const mazeRandomDirection = () => {
    const shuffle = (arr) => { // shuffle array
      let a = arr.slice();
      for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
      }
      return a;
    }
    let directions = ['up', 'right', 'down', 'left'];
    directions = shuffle(directions);
    return directions;
  }

  const randomDirection = mazeRandomDirection();
  randomDirection.map((direction) => {
    switch (direction) {
      case 'up':
      if( arrayCellIsUndefined( maze, row-2, col ) ) return;
      if( maze[row-2][col] != 0 ) {
        maze[row-2][col] = 0;
        maze[row-1][col] = 0;
        mazeRecursion(maze, col, row - 2);
      }
      break;
      case 'down':
      if( arrayCellIsUndefined( maze, row+2, col ) ) return;
      if( maze[row+2][col] != 0 ) {
        maze[row+2][col] = 0;
        maze[row+1][col] = 0;
        mazeRecursion(maze, col, row + 2);
      }
      break;
      case 'left':
      if( arrayCellIsUndefined( maze, row, col-2 ) ) return;
      if( maze[row][col-2] != 0 ) {
        maze[row][col-2] = 0;
        maze[row][col-1] = 0;
        mazeRecursion(maze, col - 2, row);
      }
      break;
      case 'right':
      if( arrayCellIsUndefined( maze, row, col+2 ) ) return;
      if( maze[row][col+2] != 0 ) {
        maze[row][col+2] = 0;
        maze[row][col+1] = 0;
        mazeRecursion(maze, col + 2, row);
      }
      break;
    }
  });
}

module.exports = {generateMaze};
