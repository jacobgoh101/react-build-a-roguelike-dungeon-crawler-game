const keyboardJS = require('keyboardJS');
import {store} from './reducer.js';

keyboardJS.bind('up', () => {
  store.dispatch({
    type: 'MOVE',
    direction: 'up'
  });
});
keyboardJS.bind('down', () => {
  store.dispatch({
    type: 'MOVE',
    direction: 'down'
  });
});
keyboardJS.bind('left', () => {
  store.dispatch({
    type: 'MOVE',
    direction: 'left'
  });
});
keyboardJS.bind('right', () => {
  store.dispatch({
    type: 'MOVE',
    direction: 'right'
  });
});

module.exports = {keyboardJS};
