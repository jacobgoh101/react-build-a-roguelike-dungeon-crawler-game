import {Redux, createStore, combineReducers, applyMiddleware} from 'redux';
const randomInt = require('random-int');
import {generateMaze} from './mazeGenerator.js';
import {darkifyAllCellsState} from './darkifyAllCellsState.js';
import {allCellsState, walkableCells, playerCell} from './mainReducer-init.js';
const _ = require('lodash');

const move = (mainReducer, direction) => {
  const allCellsState = mainReducer.allCellsState;
  const walkableCells = mainReducer.walkableCells;
  const playerCell = mainReducer.playerCell;
  const playerHealth = mainReducer.playerHealth;
  if(playerHealth <= 0) return mainReducer;
  const playerAttackPower = mainReducer.playerAttackPower
  const playerLevel = mainReducer.playerLevel;
  const playerXP = mainReducer.playerXP;
  const gameWon = mainReducer.gameWon;
  if(gameWon) return mainReducer;
  let moveToCell = null;
  let walkable = false;
  switch (direction) {
    case 'up':{
      moveToCell = { ...playerCell, row: playerCell.row - 1 };
    }break;
    case 'down':{
      moveToCell = { ...playerCell, row: playerCell.row + 1 };
    }break;
    case 'left':{
      moveToCell = { ...playerCell, col: playerCell.col - 1 };
    }break;
    case 'right':{
      moveToCell = { ...playerCell, col: playerCell.col + 1 };
    }break;
    default:
    return mainReducer;
  }

  walkableCells.map((cell) => {
    if(walkable) return;
    if( _.isEqual(moveToCell, cell) ) {
      walkable = true;
    }
  });
  if(!walkable) return mainReducer;

  // monsterCell
  if(allCellsState[moveToCell.row][moveToCell.col].indexOf('monsterCell') > -1) {
    const monsterCell = allCellsState[moveToCell.row][moveToCell.col];
    const getMonsterHealth = (monsterCell) => {
      let arr = monsterCell.split(' ');
      let result = null;
      arr.map((str) => {
        if(result) return;
        if(str.indexOf('monster-health-') > -1){
          result = str.split('-');
          result = result[result.length - 1];
        }
      });
      return Number(result);
    }
    const cutMonsterHealth = (monsterCell, cut) => {
      let arr = monsterCell.split(' ');
      arr = arr.map((str) => {
        if( str.indexOf('monster-health-') > -1 ){
          str = str.split('-');
          let monsterHealth = str[str.length-1];
          monsterHealth = Number(monsterHealth);
          monsterHealth -= Number(cut);
          str[str.length-1] = monsterHealth;
          str = str.join('-');
        }
        return str;
      });
      return arr.join(' ');
    }
    const getMonsterAttackPower = (monsterCell) => {
      let arr = monsterCell.split(' ');
      let result = null;
      arr.map((str) => {
        if(result) return;
        if(str.indexOf('monster-attack-power-') > -1){
          result = str.split('-');
          result = result[result.length - 1];
        }
      });
      return Number(result);
    }
    let monsterHealth = getMonsterHealth(monsterCell);
    const monsterAttackPower = getMonsterAttackPower(monsterCell);

    // both attack
    monsterHealth -= playerAttackPower;
    let newPlayerHealth = playerHealth - monsterAttackPower;
    let newXP = Number(playerXP) + Number(playerAttackPower < monsterHealth ? playerAttackPower : monsterHealth + playerAttackPower);
    let newPlayerLevel = Math.floor( newXP / ( 50 * Math.pow(1.1, playerLevel) ) );
    let newPlayerAttackPower = playerAttackPower;
    if(newPlayerLevel > playerLevel) newPlayerAttackPower = playerAttackPower + newPlayerLevel*11;

    if(newPlayerHealth > 0) {
      if(monsterHealth > 0) {
        let previousMonsterHealth = getMonsterHealth(monsterCell);
        let newAllCellsState = [...allCellsState];

        //monster health
        newAllCellsState[moveToCell.row][moveToCell.col] = cutMonsterHealth(newAllCellsState[moveToCell.row][moveToCell.col], previousMonsterHealth - monsterHealth);

        return {...mainReducer,
          allCellsState: newAllCellsState,
          playerHealth: newPlayerHealth,
          playerXP: newXP,
          playerLevel: newPlayerLevel,
          playerAttackPower: newPlayerAttackPower,
        }
      }else{
        let newAllCellsState = movePlayerCell(allCellsState, playerCell, moveToCell);
        newAllCellsState = darkifyAllCellsState(newAllCellsState, moveToCell);

        // monster king
        let newGameWon = gameWon;
        if( monsterCell.indexOf('monsterKingCell') > -1 ) {
          newGameWon = true;
          alert('You have won the game.')
        }

        return {...mainReducer,
          playerCell: moveToCell,
          playerXP: newXP,
          playerLevel: newPlayerLevel,
          playerAttackPower: newPlayerAttackPower,
          allCellsState: newAllCellsState,
          gameWon: newGameWon,
        }
      }
    }else {
      alert('You are dead');
      return {...mainReducer,
        playerHealth: newPlayerHealth
      }
    }
  }

  // foodCell
  if(allCellsState[moveToCell.row][moveToCell.col].indexOf('foodCell') > -1)  {
    const getFoodEnergy = (foodCell) => {
      let arr = foodCell.split(' ');
      let result = null;
      arr.map((str) => {
        if(result) return;
        if(str.indexOf('food-energy-') > -1){
          result = str.split('-');
          result = result[result.length - 1];
        }
      });
      return Number(result);
    }
    const foodCell = allCellsState[moveToCell.row][moveToCell.col];
    const foodEnergy = getFoodEnergy(foodCell);
    let newAllCellsState = movePlayerCell(allCellsState, playerCell, moveToCell);
    newAllCellsState = darkifyAllCellsState(newAllCellsState, moveToCell);
    return {...mainReducer,
      playerCell: moveToCell,
      allCellsState: newAllCellsState,
      playerHealth: playerHealth + foodEnergy
    }
  }

  // weaponCell
  if(allCellsState[moveToCell.row][moveToCell.col].indexOf('weaponCell') > -1)  {
    const getWeaponAttackPower = (weaponCell) => {
      let arr = weaponCell.split(' ');
      let result = null;
      arr.map((str) => {
        if(result) return;
        if(str.indexOf('weapon-attack-power-') > -1){
          result = str.split('-');
          result = result[result.length - 1];
        }
      });
      return Number(result);
    }
    const weaponCell = allCellsState[moveToCell.row][moveToCell.col];
    const weaponAttackPower = getWeaponAttackPower(weaponCell);
    let newAllCellsState = movePlayerCell(allCellsState, playerCell, moveToCell);
    newAllCellsState = darkifyAllCellsState(newAllCellsState, moveToCell);
    return {...mainReducer,
      playerCell: moveToCell,
      allCellsState: newAllCellsState,
      playerAttackPower: playerAttackPower + weaponAttackPower
    }
  }

  // staircaseCell
  if(allCellsState[moveToCell.row][moveToCell.col].indexOf('staircaseCell') > -1)  {
    let maze = generateMaze(20,20,moveToCell.row, moveToCell.col);
    let walkableCells = [];
    let allCellsState = maze.map((row, rowIndex) => {
      return row.map((col, colIndex) => {
        let className = "cell row-" + rowIndex + " col-" + colIndex + " ";
        if( col == 1 ) {
          className += "wall ";
        }else{
          let walkableCell = {
            row: rowIndex,
            col: colIndex
          };
          walkableCells.push(walkableCell);
        }
        if(colIndex == 0) {
          className += "clear-left ";
        }
        return className;
      })
    });

    //set special cell
    let randWalkableCells = _.shuffle(walkableCells);
    let playerCell = moveToCell;
    let monsterKingCell = randWalkableCells.shift();
    let foodCell = [];
    let monsterCell = [];
    let weaponCell = [];
    _.times(3, () => {
      foodCell.push(randWalkableCells.shift());
      monsterCell.push(randWalkableCells.shift());
      weaponCell.push(randWalkableCells.shift());
    });
    let staircaseCell = randWalkableCells.shift();
    while(staircaseCell.row%2 == 0 || staircaseCell.col%2 == 0) {
      staircaseCell = randWalkableCells.shift();
    }

    allCellsState = allCellsState.map((row, rowIndex) => {
      return row.map((cell, colIndex) => {
        if(colIndex == playerCell.col && rowIndex == playerCell.row){
          return cell+' playerCell';
        }
        if(colIndex == staircaseCell.col && rowIndex == staircaseCell.row){
          return cell+' staircaseCell';
        }
        if(colIndex == monsterKingCell.col && rowIndex == monsterKingCell.row){
          if(playerLevel >= 5){
            return cell+' monsterKingCell monsterCell monster-health-'+randomInt(800, 1000)+' monster-attack-power-'+randomInt(5,20);
          }
        }
        for (let i = 0; i < foodCell.length; i++) {
          if(colIndex == foodCell[i].col && rowIndex == foodCell[i].row){
            return cell+' foodCell food-energy-'+randomInt(30,100);
          }
        }
        for (let i = 0; i < monsterCell.length; i++) {
          if(colIndex == monsterCell[i].col && rowIndex == monsterCell[i].row){
            return cell+' monsterCell monster-health-'+randomInt(20, 50)+' monster-attack-power-'+randomInt(5,20);
          }
        }
        for (let i = 0; i < weaponCell.length; i++) {
          if(colIndex == weaponCell[i].col && rowIndex == weaponCell[i].row){
            return cell+' weaponCell weapon-attack-power-'+randomInt(5,10);
          }
        }
        for (let i = 0; i < walkableCells.length; i++) {
          if(colIndex == walkableCells[i].col && rowIndex == walkableCells[i].row){
            return cell+' normalCell';
          }
        }
        return cell;
      });
    });
    return {...mainReducer,
      playerCell: moveToCell,
      allCellsState: darkifyAllCellsState(allCellsState, moveToCell),
      walkableCells,
    }
  }

  // normalCell
  if(allCellsState[moveToCell.row][moveToCell.col].indexOf('normalCell') > -1)  {
    let newAllCellsState = movePlayerCell(allCellsState, playerCell, moveToCell);
    newAllCellsState = darkifyAllCellsState(newAllCellsState, moveToCell);
    return {...mainReducer,
      playerCell: moveToCell,
      allCellsState: newAllCellsState
    }
  }

  return mainReducer;
}

const movePlayerCell = (allCellsState, oldPlayerCell, newPlayerCell) => {
  let allCellsStateTemp = [...allCellsState];
  allCellsStateTemp[oldPlayerCell.row][oldPlayerCell.col] = allCellsStateTemp[oldPlayerCell.row][oldPlayerCell.col].replace('playerCell', '');
  allCellsStateTemp[oldPlayerCell.row][oldPlayerCell.col] += ' normalCell';
  allCellsStateTemp[newPlayerCell.row][newPlayerCell.col] =  allCellsStateTemp[newPlayerCell.row][newPlayerCell.col].replace('monsterCell', '');
  allCellsStateTemp[newPlayerCell.row][newPlayerCell.col] = allCellsStateTemp[newPlayerCell.row][newPlayerCell.col].replace('foodCell', '');
  allCellsStateTemp[newPlayerCell.row][newPlayerCell.col] = allCellsStateTemp[newPlayerCell.row][newPlayerCell.col].replace('weaponCell', '');
  allCellsStateTemp[newPlayerCell.row][newPlayerCell.col] += ' playerCell';
  return allCellsStateTemp;
}

module.exports = {move};
