const randomInt = require('random-int');
import {generateMaze} from './mazeGenerator.js';
import {darkifyAllCellsState} from './darkifyAllCellsState.js';
const _ = require('lodash');

const rowTotal = 20;
const colTotal = 20;
const maze = generateMaze(rowTotal, colTotal);
let walkableCells = [];
let allCellsState = maze.map((row, rowIndex) => {
  return row.map((col, colIndex) => {
    let className = "cell row-" + rowIndex + " col-" + colIndex + " ";
    if( col == 1 ) {
      className += "wall ";
    }else{
      let walkableCell = {
        row: rowIndex,
        col: colIndex
      };
      walkableCells.push(walkableCell);
    }
    if(colIndex == 0) {
      className += "clear-left ";
    }
    return className;
  })
});

//set special cell
let randWalkableCells = _.shuffle(walkableCells);
let playerCell = randWalkableCells.shift();
let foodCell = [];
let monsterCell = [];
let weaponCell = [];
_.times(3, () => {
  foodCell.push(randWalkableCells.shift());
  monsterCell.push(randWalkableCells.shift());
  weaponCell.push(randWalkableCells.shift());
});
let staircaseCell = randWalkableCells.shift();
while(staircaseCell.row%2 == 0 || staircaseCell.col%2 == 0) {
  staircaseCell = randWalkableCells.shift();
}

allCellsState = allCellsState.map((row, rowIndex) => {
  return row.map((cell, colIndex) => {
    if(colIndex == playerCell.col && rowIndex == playerCell.row){
      return cell+' playerCell';
    }
    if(colIndex == staircaseCell.col && rowIndex == staircaseCell.row){
      return cell+' staircaseCell';
    }
    for (let i = 0; i < foodCell.length; i++) {
      if(colIndex == foodCell[i].col && rowIndex == foodCell[i].row){
        return cell+' foodCell food-energy-'+randomInt(30,100);
      }
    }
    for (let i = 0; i < monsterCell.length; i++) {
      if(colIndex == monsterCell[i].col && rowIndex == monsterCell[i].row){
        return cell+' monsterCell monster-health-'+randomInt(20, 50)+' monster-attack-power-'+randomInt(5,20);
      }
    }
    for (let i = 0; i < weaponCell.length; i++) {
      if(colIndex == weaponCell[i].col && rowIndex == weaponCell[i].row){
        return cell+' weaponCell weapon-attack-power-'+randomInt(5,10);
      }
    }
    for (let i = 0; i < walkableCells.length; i++) {
      if(colIndex == walkableCells[i].col && rowIndex == walkableCells[i].row){
        return cell+' normalCell';
      }
    }
    return cell;
  });
});
allCellsState = darkifyAllCellsState(allCellsState, playerCell);

const playerHealth = 100;
const playerAttackPower = 10;
const playerLevel = 0;
const playerXP = 0;
const darkness = true;
const gameWon = false;

module.exports = {
  allCellsState,
  walkableCells,
  playerCell,
  playerHealth,
  playerAttackPower,
  playerLevel,
  playerXP,
  darkness,
  gameWon
}
