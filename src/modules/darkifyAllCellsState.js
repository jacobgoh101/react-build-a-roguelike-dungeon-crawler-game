const _ = require('lodash');

const darkifyAllCellsState = (allCellsState, playerCell) => {
  let allCellsStateTemp = [...allCellsState];
  let lightCells = [];
  let radius = 5;
  let i = 0 - radius;
  _.times(radius*2+1, () => {
    let j = 0 - radius;
    _.times(radius*2+1, () => {
      let cell = {
        row: playerCell.row - i,
        col: playerCell.col - j
      };
      lightCells.push(cell);
      j++;
    });
    i++;
  });
  // remove all lightCell
  allCellsStateTemp = allCellsStateTemp.map((row, rowIndex) => {
    return row.map((cell, colIndex) => {
      return cell.replace('lightCell', '');
    });
  });
  // add lightCell
  lightCells.map((cell) => {
    if( typeof allCellsStateTemp[cell.row] !== 'undefined' ){
      if( typeof allCellsStateTemp[cell.row][cell.col] !== 'undefined' ){
        allCellsStateTemp[cell.row][cell.col] += ' lightCell';
      }
    }
  });
  return allCellsStateTemp;
}

module.exports = {darkifyAllCellsState};
