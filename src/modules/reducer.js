import {Redux, createStore, combineReducers, applyMiddleware} from 'redux';
const randomInt = require('random-int');
import {generateMaze} from './mazeGenerator.js';
import {
  allCellsState,
  walkableCells,
  playerCell,
  playerHealth,
  playerAttackPower,
  playerLevel,
  playerXP,
  darkness,
  gameWon
} from './mainReducer-init.js'
import {move} from './reducer-move-functions.js';
const _ = require('lodash');


const mainReducer = (state = {
  allCellsState,
  walkableCells,
  playerCell,
  playerHealth,
  playerAttackPower,
  playerLevel,
  playerXP,
  darkness,
  gameWon
},action) => {
  switch (action.type) {
    case 'MOVE':
    return move(state, action.direction);
    break;
    case 'TOGGLEDARKNESS':
    return {
      ...state,
      darkness: !state.darkness
    }
    break;
    default:
    return state;
  }
}

const store = createStore(mainReducer);

module.exports = {store};
