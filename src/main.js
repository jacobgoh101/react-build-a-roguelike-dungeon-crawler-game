const React = require('react');
const ReactDOM = require('react-dom');
const _ = require('lodash');
import {store} from './modules/reducer.js';
import {keyboardJS} from './modules/keyboard.js';
require('./style.scss');

class App extends React.Component {
  render() {
    const mainReducer = this.props.mainReducer;
    const allCellsState = mainReducer.allCellsState;
    const playerHealth = mainReducer.playerHealth;
    const playerAttackPower = mainReducer.playerAttackPower
    const playerLevel = mainReducer.playerLevel;
    const playerXP = mainReducer.playerXP;
    const darkness = mainReducer.darkness;
    const gameWon = mainReducer.gameWon;
    const cellsOutput = [];
    allCellsState.map((row) => {
      row.map((cell) => {
        cellsOutput.push(
          <div className={cell} key={cellsOutput.length}></div>
        )
      })
    })
    let gameOverHidden = ' hidden';
    if( playerHealth <=0 ) gameOverHidden = '';

    let gameWonHidden = ' hidden';
    if( gameWon ) gameWonHidden = '';

    let darknessStyle = '';
    if(darkness) {
      darknessStyle = <style type="text/css" dangerouslySetInnerHTML={{__html: "\n.cells-container .cell.lightCell {\nbackground-color: #fff; }\n.cells-container .cell.wall {\nbackground-color: #333; }\n.cells-container .cell:not(.lightCell) {\nbackground-image: none !important;\nbackground-color: #333;}\n" }} />;
    }

    const toggleDarkness = () => {
      store.dispatch({
        type: 'TOGGLEDARKNESS'
      });
    }

    return (
      <div>
        {darknessStyle}
        <div className="player-info">
          <span>Health: {playerHealth}</span>
          <span>Attack Power: {playerAttackPower}</span>
          <span>Level: {playerLevel}</span>
          <span>XP: {playerXP}</span>
          <br />
          <br />
          <button className="btn btn-default" onClick={toggleDarkness}>Toggle Darkness</button>
          <h1 className={"text-danger text-center"+gameOverHidden}>Game Over</h1>
          <h1 className={"text-info text-center"+gameWonHidden}>You have won the game!</h1>
        </div>
        <div className="cells-container clearfix">
          {cellsOutput}
        </div>
        <h4 className="text-white">Kill <img src="./src/img/monster-king.png" style={{width: 20, height: 20}} /> to win</h4>
        <div>
        <br />
        <h6 className="text-white text-center">This is built using NodeJS and Webpack<br />Codes can be found at <a target="_blank" href="https://bitbucket.org/jacobgoh101/react-build-a-roguelike-dungeon-crawler-game">https://bitbucket.org/jacobgoh101/react-build-a-roguelike-dungeon-crawler-game</a></h6>
        <br />
        <br />
      </div>
      </div>
    )
  }
}

const render = () => {
  ReactDOM.render(<App mainReducer={store.getState()} />, document.getElementById('root'));
}

store.subscribe(render);
render();
